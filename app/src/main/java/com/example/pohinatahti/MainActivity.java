package com.example.pohinatahti;

import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<ImageView> jooseppiGoodPupus = new ArrayList<>();
    ArrayList<ImageView> jooseppiBadPupus = new ArrayList<>();
    ArrayList<ImageView> ibelsGoodPupus = new ArrayList<>();
    ArrayList<ImageView> ibelsBadPupus = new ArrayList<>();
    ArrayList<ImageView> bubuGoodPupus = new ArrayList<>();
    ArrayList<ImageView> bubuBadPupus = new ArrayList<>();
    ArrayList<ImageView> lassiGoodPupus = new ArrayList<>();
    ArrayList<ImageView> lassiBadPupus = new ArrayList<>();
    LinearLayout jooseppiPupuLayout;
    LinearLayout ibelsPupuLayout;
    LinearLayout bubuPupuLayout;
    LinearLayout lassiPupuLayout;
    int maxPupuAmount;
    int pupuWidth;
    int pupuHalfWidth;
    ConstraintLayout constraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        constraintLayout = findViewById(R.id.constraint);
        jooseppiPupuLayout = findViewById(R.id.jooseppiPupus);
        ibelsPupuLayout = findViewById(R.id.ibelsPupus);
        bubuPupuLayout = findViewById(R.id.bubuPupus);
        lassiPupuLayout = findViewById(R.id.lassiPupus);
        ViewTreeObserver viewTreeObserver = jooseppiPupuLayout.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    jooseppiPupuLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    maxPupuAmount = jooseppiPupuLayout.getHeight() / jooseppiPupuLayout.getWidth();
                    pupuWidth = jooseppiPupuLayout.getWidth();
                    pupuHalfWidth = pupuWidth / 2;
                }
            });
        }
        jooseppiPupuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (jooseppiBadPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.pupu), jooseppiGoodPupus, jooseppiPupuLayout, jooseppiPupuLayout.getLeft(), R.drawable.pupu);
                } else {
                    deleteBunny(jooseppiBadPupus, jooseppiPupuLayout);
                }
            }
        });
        jooseppiPupuLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (jooseppiGoodPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.toimisaatana), jooseppiBadPupus, jooseppiPupuLayout, jooseppiPupuLayout.getLeft(), R.drawable.toimisaatana);
                } else {
                    deleteBunny(jooseppiGoodPupus, jooseppiPupuLayout);
                }
                return true;
            }
        });
        ibelsPupuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ibelsBadPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.pupu), ibelsGoodPupus, ibelsPupuLayout, ibelsPupuLayout.getLeft(), R.drawable.pupu);
                } else {
                    deleteBunny(ibelsBadPupus, ibelsPupuLayout);
                }
            }
        });
        ibelsPupuLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (ibelsGoodPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.toimisaatana), ibelsBadPupus, ibelsPupuLayout, ibelsPupuLayout.getLeft(), R.drawable.toimisaatana);
                } else {
                    deleteBunny(ibelsGoodPupus, ibelsPupuLayout);
                }
                return true;
            }
        });
        bubuPupuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bubuBadPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.pupu), bubuGoodPupus, bubuPupuLayout, bubuPupuLayout.getLeft(), R.drawable.pupu);
                } else {
                    deleteBunny(bubuBadPupus, bubuPupuLayout);
                }
            }
        });
        bubuPupuLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (bubuGoodPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.toimisaatana), bubuBadPupus, bubuPupuLayout, bubuPupuLayout.getLeft(), R.drawable.toimisaatana);
                } else {
                    deleteBunny(bubuGoodPupus, bubuPupuLayout);
                }
                return true;
            }
        });
        lassiPupuLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lassiBadPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.pupu), lassiGoodPupus, lassiPupuLayout, lassiPupuLayout.getLeft(), R.drawable.pupu);
                } else {
                    deleteBunny(lassiBadPupus, lassiPupuLayout);
                }
            }
        });
        lassiPupuLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (lassiGoodPupus.size() == 0) {
                    addBunny(createImageView(R.drawable.toimisaatana), lassiBadPupus, lassiPupuLayout, lassiPupuLayout.getLeft(), R.drawable.toimisaatana);
                } else {
                    deleteBunny(lassiGoodPupus, lassiPupuLayout);
                }
                return true;
            }
        });
    }

    private ImageView createImageView(int drawable) {
        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setBackground(getApplicationContext().getDrawable(drawable));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(pupuWidth, pupuWidth));
        imageView.setId(View.generateViewId());
        return imageView;
    }

    private void addBunny(ImageView imageView, ArrayList<ImageView> pupuList, LinearLayout pupuLayout, int margin, int pupu) {
        if (pupuList.size() >= maxPupuAmount) {
            if (pupuList.size() / 5 > 0 && pupuList.size() <= maxPupuAmount + maxPupuAmount * 4 - 1) {
                if ((pupuList.size() - 5) % 4 == 0) {
                    pupuList.get((pupuList.size() - 5) / 4).setVisibility(View.INVISIBLE);
                    imageView.setLayoutParams(new ConstraintLayout.LayoutParams(pupuHalfWidth, pupuHalfWidth));
                    constraintLayout.addView(imageView);
                    ConstraintSet set = new ConstraintSet();
                    set.clone(constraintLayout);
                    set.connect(imageView.getId(), ConstraintSet.TOP, R.id.pupus, ConstraintSet.TOP, pupuWidth * (pupuList.size() - 5) / 4);
                    set.connect(imageView.getId(), ConstraintSet.START, R.id.pupus, ConstraintSet.START, margin);
                    set.applyTo(constraintLayout);
                    pupuList.add(imageView);
                    imageView = new ImageView(getApplicationContext());
                    imageView.setBackground(getApplicationContext().getDrawable(pupu));
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(pupuWidth, pupuWidth));
                    imageView.setId(View.generateViewId());
                }
                imageView.setLayoutParams(new ConstraintLayout.LayoutParams(pupuHalfWidth, pupuHalfWidth));
                constraintLayout.addView(imageView);
                ConstraintSet set = new ConstraintSet();
                set.clone(constraintLayout);
                if ((pupuList.size() - 5) % 4 == 1) {
                    set.connect(imageView.getId(), ConstraintSet.TOP, pupuList.get(pupuList.size() - 1).getId(), ConstraintSet.TOP, 0);
                    set.connect(imageView.getId(), ConstraintSet.START, pupuList.get(pupuList.size() - 1).getId(), ConstraintSet.START, pupuHalfWidth);
                } else if ((pupuList.size() - 5) % 4 == 2) {
                    set.connect(imageView.getId(), ConstraintSet.TOP, pupuList.get(pupuList.size() - 2).getId(), ConstraintSet.BOTTOM, 0);
                    set.connect(imageView.getId(), ConstraintSet.START, pupuList.get(pupuList.size() - 2).getId(), ConstraintSet.START, 0);
                } else {
                    set.connect(imageView.getId(), ConstraintSet.TOP, pupuList.get(pupuList.size() - 2).getId(), ConstraintSet.BOTTOM, 0);
                    set.connect(imageView.getId(), ConstraintSet.START, pupuList.get(pupuList.size() - 2).getId(), ConstraintSet.START, 0);
                }
                set.applyTo(constraintLayout);
                pupuList.add(imageView);
            }
        } else if (pupuList.size() < maxPupuAmount) {
            pupuList.add(imageView);
            pupuLayout.addView(imageView);
        }
    }

    private void deleteBunny(ArrayList<ImageView> pupuList, LinearLayout pupuLayout) {
        if (pupuList.size() > maxPupuAmount) {
            Log.d("Test", Integer.toString(pupuList.size()));
            if ((pupuList.size() - 5) % 4 == 2) {
                Log.d("Test", "OK");
                constraintLayout.removeView(pupuList.remove(pupuList.size() - 1));
                constraintLayout.removeView(pupuList.remove(pupuList.size() - 1));
                for (int i = pupuList.size() - 1; i >= 0; i--) {
                    if (pupuList.get(i).getVisibility() == View.INVISIBLE) {
                        pupuList.get(i).setVisibility(View.VISIBLE);
                        break;
                    }
                }
            } else {
                constraintLayout.removeView(pupuList.remove(pupuList.size() - 1));
            }
        } else if (!pupuList.isEmpty()){
            pupuLayout.removeView(pupuList.remove(pupuList.size() - 1));
        }
    }
}
